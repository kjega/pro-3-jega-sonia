const express = require('express')
const axios = require('axios')
const bodyParser = require('body-parser')
const  port = 8088;

const app = express();

app.set('view engine', 'ejs')

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use('/', express.static('public'))


app.get('/', (req, res) =>{
  res.render('contact.ejs');
})

app.post('/', (req, res) => {
  // console.log(req.body);
  axios.post('http://localhost:8089/user/new', {data : req.body})
  .then(function (resp) {
    res.send();
  })
  .catch(function (error) {
  });
});



// app.get('/list', (req, res) =>{
//   axios.get('http://localhost:8089/user' )
  
//   .then(function(response) {
//     // Je dois rendre mes donnees en string puis en objet pour pouvoir acceder
//     let tableauUsers = JSON.parse(JSON.stringify(response.data.users));
//     res.render("list.ejs", { users: tableauUsers });
//   })

//   .catch(err =>{
//     console.log(err);
//   })
// })

app.get("/list", (req, res) => {
    
  axios
    .get("http://localhost:8089/users")
    .then(function(response) {
    
      let tableauUsers = JSON.parse(JSON.stringify(response.data.users));
      res.render("list.ejs", { users: tableauUsers });
    })
    .catch(function(error) {
         console.log(error);
    })
    .finally(function() {
    
    });
});

app.listen(port);